export const part1 = ([publicDoor, publicCard]) => {
  publicDoor = parseInt(publicDoor, 10);
  publicCard = parseInt(publicCard, 10);

  function findLoop(subject, key) {
    let value = 1;
    for (let i = 1; i < Number.MAX_SAFE_INTEGER; i++) {
      value *= subject;
      value %= 20201227;
      if (key === value) {
        return i;
      }
    }
  }

  function encrypt(loop, subject) {
    let value = 1;
    for (let i = 0; i < loop; i++) {
      value *= subject;
      value %= 20201227;
    }
    return value;
  }

  return encrypt(findLoop(7, publicCard), findLoop(7, publicDoor));
};

export const part2 = () => {
  return 'Merry XMAS!';
};
