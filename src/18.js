function tokenize(inputLines) {
  return inputLines.map((line) => {
    const [...tokens] = line
      .match(/(\d+|\+|\*|\(|\))/g)
      .map((v) => (/\d+/.test(v) ? parseInt(v, 10) : v));
    return tokens;
  });
}

function extractExpression(tokens, fromIndex) {
  let j = fromIndex + 1;
  let level = 1;
  while (level > 0) {
    if (tokens[j] === '(') level += 1;
    else if (tokens[j] === ')') level -= 1;
    j++;
  }
  return tokens.slice(fromIndex + 1, j - 1);
}

function calc(tokens) {
  let result = 0;
  let operation = '+';
  for (let i = 0; i < tokens.length; i++) {
    if (tokens[i] === '(') {
      const subExpression = extractExpression(tokens, i);
      i += subExpression.length + 1;
      if (operation === '*') {
        result *= calc(subExpression);
      } else if (operation === '+') {
        result += calc(subExpression);
      }
    } else if (tokens[i] === '*' || tokens[i] === '+') {
      operation = tokens[i];
    } else {
      if (operation === '*') {
        result *= tokens[i];
      } else if (operation === '+') {
        result += tokens[i];
      }
    }
  }
  return result;
}

function advancedCalc(tokens) {
  while (tokens.length > 1) {
    if (tokens.includes('(')) {
      const index = tokens.indexOf('(');
      const subExpression = extractExpression(tokens, index);
      tokens.splice(
        index,
        subExpression.length + 2,
        advancedCalc(subExpression)
      );
    } else if (tokens.includes('+')) {
      const index = tokens.indexOf('+');
      tokens.splice(index - 1, 3, tokens[index - 1] + tokens[index + 1]);
    } else {
      tokens.splice(0, 3, tokens[0] * tokens[2]);
    }
  }
  return tokens[0];
}

export const part1 = (inputLines) => {
  const tokenizedExpressions = tokenize(inputLines);
  return tokenizedExpressions.reduce(
    (total, expression) => total + calc(expression),
    0
  );
};

export const part2 = (inputLines) => {
  const tokenizedExpressions = tokenize(inputLines);
  return tokenizedExpressions.reduce(
    (total, expression) => total + advancedCalc(expression),
    0
  );
};
