import { part1, part2 } from './11';

describe('Day 11 - Part 1', () => {
  it(`returns the number of occupied seats`, () => {
    expect(
      part1([
        'L.LL.LL.LL',
        'LLLLLLL.LL',
        'L.L.L..L..',
        'LLLL.LL.LL',
        'L.LL.LL.LL',
        'L.LLLLL.LL',
        '..L.L.....',
        'LLLLLLLLLL',
        'L.LLLLLL.L',
        'L.LLLLL.LL',
      ])
    ).toBe(37);
  });
});

describe('Day 11 - Part 2', () => {
  it(`returns total number of occupied seats`, () => {
    expect(
      part2([
        'L.LL.LL.LL',
        'LLLLLLL.LL',
        'L.L.L..L..',
        'LLLL.LL.LL',
        'L.LL.LL.LL',
        'L.LLLLL.LL',
        '..L.L.....',
        'LLLLLLLLLL',
        'L.LLLLLL.L',
        'L.LLLLL.LL',
      ])
    ).toBe(26);
  });
});
