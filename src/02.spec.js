import { part1, part2 } from './02';

describe('Day 02 - Part 1', () => {
  it(`gets total valid passwords`, () => {
    expect(part1(['1-3 a: abcde', '1-3 b: cdefg', '2-9 c: ccccccccc'])).toBe(2);
  });
});

describe('Day 02 - Part 2', () => {
  it(`gets total valid passwords`, () => {
    expect(part2(['1-3 a: abcde', '1-3 b: cdefg', '2-9 c: ccccccccc'])).toBe(1);
  });
});
