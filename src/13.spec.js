import { part1, part2 } from './13';

describe('Day 13 - Part 1', () => {
  it(`returns ID of earliest bus multiplied by the minutes waiting`, () => {
    expect(part1(['939', '7,13,x,x,59,x,31,19'])).toBe(295);
  });
});

describe('Day 13 - Part 2', () => {
  it(`returns earliest timestamp`, () => {
    expect(part2(['1', '17,x,13,19'])).toBe(3417);
  });

  it(`returns earliest timestamp`, () => {
    expect(part2(['1', '67,7,59,61'])).toBe(754018);
  });
  it(`returns earliest timestamp`, () => {
    expect(part2(['1', '67,x,7,59,61'])).toBe(779210);
  });
  it(`returns earliest timestamp`, () => {
    expect(part2(['1', '67,7,x,59,61'])).toBe(1261476);
  });
  it(`returns earliest timestamp`, () => {
    expect(part2(['1', '1789,37,47,1889'])).toBe(1202161486);
  });
});
