function toGroups(input) {
  const groups = [];
  let group = [];
  for (const line of input) {
    if (line === '') {
      groups.push(group);
      group = [];
    } else {
      group.push(line);
    }
  }
  if (group.length) {
    groups.push(group);
  }
  return groups;
}

function getUniqueAnswers(group) {
  return Array.from(new Set(group.join('').split('')));
}

function sumAnyoneYeses(groups) {
  return groups.reduce((total, group) => {
    const uniqueAnswers = getUniqueAnswers(group);
    return total + uniqueAnswers.length;
  }, 0);
}

function getTotalEveryoneYeses(group, uniqueAnswers) {
  let total = 0;
  for (const answer of uniqueAnswers) {
    const everyoneAnswered = group.every((answers) => answers.includes(answer));
    if (everyoneAnswered) {
      total++;
    }
  }
  return total;
}

function sumEveryoneYeses(groups) {
  return groups.reduce((total, group) => {
    const uniqueAnswers = getUniqueAnswers(group);
    return total + getTotalEveryoneYeses(group, uniqueAnswers);
  }, 0);
}

export const part1 = (inputLines) => {
  const groups = toGroups(inputLines);
  return sumAnyoneYeses(groups);
};

export const part2 = (inputLines) => {
  const groups = toGroups(inputLines);
  return sumEveryoneYeses(groups);
};
