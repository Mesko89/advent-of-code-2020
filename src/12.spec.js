import { part1, part2 } from './12';

describe('Day 12 - Part 1', () => {
  it(`returns manhattan distance from starting position`, () => {
    expect(part1(['F10', 'N3', 'F7', 'R90', 'F11'])).toBe(25);
  });

  it(`returns manhattan distance from starting position`, () => {
    expect(
      part1(['F10', 'L180', 'F20', 'L90', 'L270', 'R90', 'R270', 'R180', 'F10'])
    ).toBe(0);
  });
});

describe('Day 12 - Part 2', () => {
  it(`returns manhattan distance from starting position`, () => {
    expect(part2(['F10', 'N3', 'F7', 'R90', 'F11'])).toBe(286);
  });
});
