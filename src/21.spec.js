import { part1, part2 } from './21';

describe('Day 21 - Part 1', () => {
  it(`returns number of times ingredients with no allergens appear`, () => {
    expect(
      part1([
        'mxmxvkd kfcds sqjhc nhms (contains dairy, fish)',
        'trh fvjkl sbzzf mxmxvkd (contains dairy)',
        'sqjhc fvjkl (contains soy)',
        'sqjhc mxmxvkd sbzzf (contains fish)',
      ])
    ).toBe(5);
  });
});

describe('Day 21 - Part 2', () => {
  it(`returns canonical dangerous ingredient list`, () => {
    expect(
      part2([
        'mxmxvkd kfcds sqjhc nhms (contains dairy, fish)',
        'trh fvjkl sbzzf mxmxvkd (contains dairy)',
        'sqjhc fvjkl (contains soy)',
        'sqjhc mxmxvkd sbzzf (contains fish)',
      ])
    ).toBe('mxmxvkd,sqjhc,fvjkl');
  });
});
