function parseInput(inputLines) {
  inputLines = inputLines.reverse();
  const rules = {};
  const inputs = [];
  let line = '';
  while ((line = inputLines.pop())) {
    const [ruleId, rest] = line.split(': ');
    if (/"[ab]"/.test(rest)) {
      rules[ruleId] = { value: rest.replace(/"/g, '') };
    } else {
      const connections = rest.split(' | ').map((v) => v.split(' '));
      rules[ruleId] = { connections };
    }
  }

  while ((line = inputLines.pop())) {
    inputs.push(line);
  }

  return { rules, inputs };
}

const MAX_DEPTH = 15;
function buildRegexString(rules, ruleId = '0', level = 0) {
  if (level > MAX_DEPTH) return '';
  const { value, connections } = rules[ruleId];
  if (value) return value;
  else
    return `(?:${connections
      .map((v) => v.map((v) => buildRegexString(rules, v, level + 1)).join(''))
      .join('|')})`;
}

export const part1 = ([...inputLines]) => {
  const { rules, inputs } = parseInput(inputLines);
  const rxString = buildRegexString(rules);
  const rx = new RegExp('^' + rxString + '$');
  return inputs.filter((i) => rx.test(i)).length;
};

export const part2 = ([...inputLines]) => {
  const { rules, inputs } = parseInput(inputLines);
  rules['8'] = { connections: [['42'], ['42', '8']] };
  rules['11'] = {
    connections: [
      ['42', '31'],
      ['42', '11', '31'],
    ],
  };
  const rxString = buildRegexString(rules);
  const rx = new RegExp('^' + rxString + '$');
  return inputs.filter((i) => rx.test(i)).length;
};
