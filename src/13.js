export const part1 = ([startingTimeString, busString]) => {
  const startingTime = parseInt(startingTimeString, 10);
  const busIds = busString
    .split(',')
    .map((v) => parseInt(v, 10))
    .filter((v) => !Number.isNaN(v));

  const calcWaiting = (busId) => busId - (startingTime % busId);

  let minWaiting = { bus: busIds[0], waiting: calcWaiting(busIds[0]) };
  for (let i = 1; i < busIds.length; i++) {
    const waiting = calcWaiting(busIds[i]);
    if (waiting < minWaiting.waiting) {
      minWaiting = { bus: busIds[i], waiting };
    }
  }

  return minWaiting.bus * minWaiting.waiting;
};

export const part2 = ([_, busString]) => {
  const buses = busString
    .split(',')
    .map((v, i) => [parseInt(v, 10), i])
    .filter(([v]) => !Number.isNaN(v));

  let time = 0;
  let step = 1;

  for (const [busId, index] of buses) {
    while ((time + index) % busId !== 0) {
      time += step;
    }
    step *= busId;
  }

  return time;
};
