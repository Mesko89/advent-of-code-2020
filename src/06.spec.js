import { part1, part2 } from './06';

describe('Day 06 - Part 1', () => {
  it(`gets total sum of anyone yeses per group`, () => {
    expect(
      part1([
        'abc',
        '',
        'a',
        'b',
        'c',
        '',
        'ab',
        'ac',
        '',
        'a',
        'a',
        'a',
        'a',
        '',
        'b',
      ])
    ).toBe(11);
  });
});

describe('Day 06 - Part 2', () => {
  it(`gets total sum of everyone yeses per group`, () => {
    expect(
      part2([
        'abc',
        '',
        'a',
        'b',
        'c',
        '',
        'ab',
        'ac',
        '',
        'a',
        'a',
        'a',
        'a',
        '',
        'b',
      ])
    ).toBe(6);
  });
});
