function toRule(line) {
  const [_, min, max, char, password] = line.match(/(\d+)-(\d+) (\w): (.*)/);
  return { rule: { char, min, max }, password };
}

function getValidPasswords(rules) {
  return rules.filter(({ rule: { char, min, max }, password }) => {
    const totalChars = password.split('').filter((c) => c === char).length;
    return totalChars >= min && totalChars <= max;
  });
}

function getTrulyValidPasswords(rules) {
  return rules.filter(({ rule: { char, pos1, pos2 }, password }) => {
    const char1 = password[pos1 - 1];
    const char2 = password[pos2 - 1];
    return char1 !== char2 && (char === char1 || char === char2);
  });
}

function toProperRule(line) {
  const [_, pos1, pos2, char, password] = line.match(/(\d+)-(\d+) (\w): (.*)/);
  return { rule: { char, pos1, pos2 }, password };
}

export const part1 = (inputLines) => {
  const rules = inputLines.map((v) => toRule(v));
  return getValidPasswords(rules).length;
};

export const part2 = (inputLines) => {
  const rules = inputLines.map((v) => toProperRule(v));
  return getTrulyValidPasswords(rules).length;
};
