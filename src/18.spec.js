import { part1, part2 } from './18';

describe('Day 18 - Part 1', () => {
  const tests = [
    { input: ['2 * 3 + (4 * 5)'], result: 26 },
    { input: ['5 + (8 * 3 + 9 + 3 * 4 * 3)'], result: 437 },
    { input: ['5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))'], result: 12240 },
    {
      input: ['((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2'],
      result: 13632,
    },
  ];
  for (const { input, result } of tests) {
    it(`returns result of "${input}"`, () => {
      expect(part1(input)).toBe(result);
    });
  }
});

describe('Day 18 - Part 2', () => {
  const tests = [
    { input: ['1 + (2 * 3) + (4 * (5 + 6))'], result: 51 },
    { input: ['2 * 3 + (4 * 5)'], result: 46 },
    { input: ['5 + (8 * 3 + 9 + 3 * 4 * 3)'], result: 1445 },
    { input: ['5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))'], result: 669060 },
    {
      input: ['((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2'],
      result: 23340,
    },
  ];
  for (const { input, result } of tests) {
    it(`returns advanced result of "${input}"`, () => {
      expect(part2(input)).toBe(result);
    });
  }
});
