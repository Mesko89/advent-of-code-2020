function isValid(number, buffer) {
  const compliments = new Set();
  for (let i = 0; i < buffer.length; i++) {
    if (compliments.has(buffer[i])) {
      return true;
    }
    const compliment = number - buffer[i];
    compliments.add(compliment);
  }
  return false;
}

function findFirstInvalidNumber(numbers, preamble) {
  if (numbers.length <= preamble) return null;
  let currentIndex = preamble;
  while (currentIndex < numbers.length) {
    if (
      !isValid(
        numbers[currentIndex],
        numbers.slice(currentIndex - preamble, currentIndex)
      )
    ) {
      return numbers[currentIndex];
    }
    currentIndex++;
  }
  return null;
}

function findContiguousSet(numbers, sum) {
  for (let i = 0; i < numbers.length; i++) {
    const set = [];
    let setTotal = 0;
    for (let j = i; j < numbers.length; j++) {
      set.push(numbers[j]);
      setTotal += numbers[j];
      if (setTotal === sum) {
        return set;
      } else if (setTotal > sum) {
        break;
      }
    }
  }
}

export const part1 = (inputLines, preamble = 25) => {
  const numbers = inputLines.map((v) => parseInt(v, 10));
  return findFirstInvalidNumber(numbers, preamble);
};

export const part2 = (inputLines, preamble = 25) => {
  const numbers = inputLines.map((v) => parseInt(v, 10));
  const firstInvalidNumber = findFirstInvalidNumber(numbers, preamble);
  const contiguousSet = findContiguousSet(numbers, firstInvalidNumber);
  return Math.min(...contiguousSet) + Math.max(...contiguousSet);
};
