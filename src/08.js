function toInstruction(line) {
  const [operation, argumentString] = line.split(' ');
  return { operation, argument: parseInt(argumentString.replace('+', ''), 10) };
}

const continues = (state) => ({ ...state, pointer: state.pointer + 1 });
const operations = {
  nop: (state) => continues(state),
  acc: (state, instruction) =>
    continues({
      ...state,
      accumulator: state.accumulator + instruction.argument,
    }),
  jmp: (state, instruction) => ({
    ...state,
    pointer: state.pointer + instruction.argument,
  }),
};

function Console(inputLines) {
  const instructions = inputLines.map((line) => toInstruction(line));
  const state = {
    pointer: 0,
    accumulator: 0,
    isLooped: false,
  };
  return {
    run({ doSingleInstructionRun }) {
      let runningState = { ...state };
      const visitedPointers = new Set();
      while (runningState.pointer < instructions.length) {
        if (doSingleInstructionRun === true) {
          if (visitedPointers.has(runningState.pointer)) {
            return { ...runningState, isLooped: true };
          }
        }
        visitedPointers.add(runningState.pointer);
        const instruction = instructions[runningState.pointer];
        runningState = operations[instruction.operation](
          runningState,
          instruction
        );
      }
      return runningState;
    },
  };
}

function generatePossiblePrograms(inputLines) {
  const indexes = inputLines
    .map((line, i) => (line.includes('nop') || line.includes('jmp') ? i : null))
    .filter((v) => v !== null);
  const programs = [];
  for (const index of indexes) {
    const possibleProgramNop = [...inputLines];
    possibleProgramNop[index] = [
      'nop',
      possibleProgramNop[index].split(' ')[1],
    ].join(' ');
    programs.push(possibleProgramNop);
    const possibleProgramJmp = [...inputLines];
    possibleProgramJmp[index] = [
      'jmp',
      possibleProgramJmp[index].split(' ')[1],
    ].join(' ');
    programs.push(possibleProgramJmp);
  }
  return programs;
}

export const part1 = (inputLines) => {
  const console = Console(inputLines);
  const state = console.run({ doSingleInstructionRun: true });
  return state.accumulator;
};

export const part2 = (inputLines) => {
  const programs = generatePossiblePrograms(inputLines);
  for (const program of programs) {
    const console = Console(program);
    const state = console.run({ doSingleInstructionRun: true });
    if (state.isLooped === false) {
      return state.accumulator;
    }
  }
};
