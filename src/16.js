function TicketSystem(inputLines) {
  const fields = [];
  const ticket = [];
  const nearbyTickets = [];

  function toField(line) {
    const [_, field, startA, endA, startB, endB] = line.match(
      /(.*): (\d+)-(\d+) or (\d+)-(\d+)/
    );
    return {
      field,
      intervals: [
        [parseInt(startA), parseInt(endA)],
        [parseInt(startB), parseInt(endB)],
      ],
    };
  }

  inputLines.reverse();
  let line = '';
  while ((line = inputLines.pop())) {
    fields.push(toField(line));
  }

  inputLines.pop(); // your ticket:
  line = inputLines.pop();
  inputLines.pop(); // ''

  ticket.push(...line.split(',').map((v) => parseInt(v, 10)));

  inputLines.pop(); // nearby tickets:
  while ((line = inputLines.pop())) {
    nearbyTickets.push(line.split(',').map((v) => parseInt(v, 10)));
  }

  const intervals = fields.map((f) => f.intervals).flat();

  return {
    fields,
    ticket,
    nearbyTickets,
    intervals,
    isValidTicket(ticket) {
      for (const value of ticket) {
        const isValidValue = intervals.some(
          ([from, to]) => from <= value && value <= to
        );
        if (!isValidValue) {
          return false;
        }
      }
      return true;
    },
  };
}

function findScanningErrorRate({ intervals, nearbyTickets }) {
  let errorRate = 0;
  for (const ticket of nearbyTickets) {
    for (const value of ticket) {
      const isValidValue = intervals.some(
        ([from, to]) => from <= value && value <= to
      );
      if (!isValidValue) {
        errorRate += value;
      }
    }
  }
  return errorRate;
}

export const part1 = ([...inputLines]) => {
  const ticketSystem = TicketSystem(inputLines);
  return findScanningErrorRate(ticketSystem);
};

export const part2 = ([...inputLines], multiplyFields) => {
  const ticketSystem = TicketSystem(inputLines);
  const validTickets = ticketSystem.nearbyTickets.filter((t) =>
    ticketSystem.isValidTicket(t)
  );
  let validFields = [];
  for (let i = 0; i < ticketSystem.ticket.length; i++) {
    const validFieldsForIndex = ticketSystem.fields.filter((field) => {
      for (const ticket of validTickets) {
        const value = ticket[i];
        const isValid = field.intervals.some(
          ([from, to]) => from <= value && value <= to
        );
        if (!isValid) return false;
      }
      return true;
    });
    validFields.push(validFieldsForIndex.map((f) => f.field));
  }

  const foundFields = [...validFields].fill(null);

  while (validFields.some((f) => f.length === 1)) {
    const index = validFields.findIndex((f) => f.length === 1);
    const field = validFields[index][0];
    foundFields[index] = field;
    validFields = validFields.map((f) => f.filter((ff) => ff !== field));
  }

  if (!multiplyFields) {
    multiplyFields = foundFields.filter((f) => f.startsWith('departure'));
  }

  return multiplyFields
    .map(
      (field) => ticketSystem.ticket[foundFields.findIndex((f) => f === field)]
    )
    .reduce((a, b) => a * b);
};
