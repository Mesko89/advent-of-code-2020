function Cups([...values]) {
  const nodeMap = {};
  let current = null;

  let prev = null;
  for (const value of values) {
    if (current === null) {
      current = { value, next: null, prev: null };
      nodeMap[value] = current;
      prev = current;
    } else {
      nodeMap[value] = { value, prev, next: null };
      prev.next = nodeMap[value];
      prev = nodeMap[value];
    }
  }

  prev.next = current;
  current.prev = prev;

  return {
    get current() {
      return current;
    },
    maxValue: Object.keys(nodeMap).length,
    getNode(value) {
      return nodeMap[value];
    },
    next() {
      current = current.next;
    },
    take() {
      const parts = current.next;
      current.next = current.next.next.next.next;
      current.next.prev = current;
      parts.next.next.next = null;
      parts.prev = null;
      return parts;
    },
    put(value, parts) {
      const node = nodeMap[value];
      node.next.prev = parts.next.next;
      parts.next.next.next = node.next;
      node.next = parts;
      parts.prev = node;
    },
  };
}

function toLabel(node, times = 9) {
  const parts = [];
  while (node && times--) {
    parts.push(node.value);
    node = node.next;
  }
  return parts.join('');
}

function simulateStep(cups) {
  const parts = cups.take();
  let nextValue =
    cups.current.value === 1 ? cups.maxValue : cups.current.value - 1;
  while (
    nextValue === parts.value ||
    nextValue === parts.next.value ||
    nextValue === parts.next.next.value
  ) {
    nextValue = nextValue === 1 ? cups.maxValue : nextValue - 1;
  }
  cups.put(nextValue, parts);
  cups.next();
}

export const part1 = ([input], moves = 100) => {
  const cups = Cups(input.split('').map((v) => parseInt(v, 10)));
  while (moves--) {
    simulateStep(cups);
  }
  const label = toLabel(cups.current, input.length);
  const indexOf1 = label.indexOf('1');
  return label.substring(indexOf1 + 1) + label.substring(0, indexOf1);
};

export const part2 = ([input], moves = 10000000) => {
  input = input.split('').map((v) => parseInt(v, 10));
  input = Array.from({ length: 1000000 }).map((_, i) =>
    i < input.length ? input[i] : i + 1
  );
  const cups = Cups(input);
  while (moves--) {
    simulateStep(cups);
  }

  const node = cups.getNode(1);
  return node.next.value * node.next.next.value;
};
