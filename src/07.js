function toBag(line) {
  const [bagColor, containString] = line.split(' bags contain ');
  if (containString === 'no other bags.') {
    return { bagColor, contains: [] };
  }
  const contains = [];
  for (const containBagString of containString.replace(/\.$/, '').split(', ')) {
    const [_, total, bagColor] = containBagString.match(
      /(\d+) ([\w\s]+) bags?/
    );
    contains.push({ total, bagColor });
  }
  return { bagColor, contains };
}

function BagStore(bags) {
  const store = bags.reduce((obj, bag) => {
    return { ...obj, [bag.bagColor]: bag };
  }, {});
  return {
    getBag(bagColor) {
      return store[bagColor];
    },
    bagContains(bag, bagColor) {
      return bag.contains.some((bagContainer) => {
        const bag = this.getBag(bagContainer.bagColor);
        if (bag.bagColor === bagColor) return true;
        else return this.bagContains(bag, bagColor);
      });
    },
    totalBagsInBag(bag) {
      return bag.contains.reduce(
        (total, bagContainer) =>
          total +
          bagContainer.total *
            (1 + this.totalBagsInBag(this.getBag(bagContainer.bagColor))),
        0
      );
    },
  };
}

export const part1 = (inputLines) => {
  const bags = inputLines.map((line) => toBag(line));
  const store = BagStore(bags);
  return bags.filter((bag) => store.bagContains(bag, 'shiny gold')).length;
};

export const part2 = (inputLines) => {
  const bags = inputLines.map((line) => toBag(line));
  const store = BagStore(bags);
  return store.totalBagsInBag(store.getBag('shiny gold'));
};
