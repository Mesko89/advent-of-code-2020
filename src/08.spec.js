import { part1, part2 } from './08';

describe('Day 08 - Part 1', () => {
  it(`gets value of accumulator at infinite loop`, () => {
    expect(
      part1([
        'nop +0',
        'acc +1',
        'jmp +4',
        'acc +3',
        'jmp -3',
        'acc -99',
        'acc +1',
        'jmp -4',
        'acc +6',
      ])
    ).toBe(5);
  });
});

describe('Day 08 - Part 2', () => {
  it(`gets total numbers of bags in shiny bag`, () => {
    expect(
      part2([
        'nop +0',
        'acc +1',
        'jmp +4',
        'acc +3',
        'jmp -3',
        'acc -99',
        'acc +1',
        'jmp -4',
        'acc +6',
      ])
    ).toBe(8);
  });
});
