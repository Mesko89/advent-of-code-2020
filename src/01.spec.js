import { part1, part2 } from './01';

describe('Day 01 - Part 1', () => {
  it(`gets multiplier of two values that add to 2020`, () => {
    expect(part1(['1721', '979', '366', '299', '675', '1456'])).toBe(514579);
  });
});

describe('Day 01 - Part 2', () => {
  it(`gets multiplier of three values that add to 2020`, () => {
    expect(part2(['1721', '979', '366', '299', '675', '1456'])).toBe(241861950);
  });
});
