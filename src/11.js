const EMPTY = 'L';
const OCCUPIED = '#';
const SPACE = '.';

function toSeatLayout(inputLines, { useSight = false } = {}) {
  const seats = inputLines.map((line) => line.split(''));

  function get(x, y) {
    return seats[y]?.[x];
  }

  function getDirectNeighborSeats(x, y) {
    return [
      { x: x - 1, y: y - 1 },
      { x: x, y: y - 1 },
      { x: x + 1, y: y - 1 },
      { x: x - 1, y: y },
      { x: x + 1, y: y },
      { x: x - 1, y: y + 1 },
      { x: x, y: y + 1 },
      { x: x + 1, y: y + 1 },
    ]
      .map(({ x, y }) => get(x, y))
      .filter((v) => v && v !== SPACE);
  }

  function getVisibleSeats(x, y) {
    const directions = [
      { x: -1, y: -1 },
      { x: 0, y: -1 },
      { x: 1, y: -1 },
      { x: -1, y: 0 },
      { x: 1, y: 0 },
      { x: -1, y: 1 },
      { x: 0, y: 1 },
      { x: 1, y: 1 },
    ];
    return directions
      .map((direction) => {
        let dx = direction.x;
        let dy = direction.y;
        let v = get(x + dx, y + dy);
        while (v === SPACE) {
          dx += direction.x;
          dy += direction.y;
          v = get(x + dx, y + dy);
        }
        return v;
      })
      .filter((v) => v && v !== SPACE);
  }

  return {
    stateKey() {
      return seats.map((line) => line.join('')).join('|');
    },
    clone() {
      return toSeatLayout(
        seats.map((line) => line.join('')),
        { useSight }
      );
    },
    get,
    getNeighborValues(x, y) {
      return useSight ? getVisibleSeats(x, y) : getDirectNeighborSeats(x, y);
    },
    shouldSeatBeEmpty(x, y) {
      const v = get(x, y);
      if (v !== OCCUPIED) return false;
      const requiredOccupiedSeats = useSight ? 5 : 4;
      return (
        this.getNeighborValues(x, y).filter((v) => v === OCCUPIED).length >=
        requiredOccupiedSeats
      );
    },
    shouldSeatBeOccupied(x, y) {
      const v = get(x, y);
      if (v !== EMPTY) return false;
      return !this.getNeighborValues(x, y).some((v) => v === OCCUPIED);
    },
    set(x, y, value) {
      seats[y][x] = value;
    },
    width: seats[0].length,
    height: seats.length,
    print() {
      console.log(`\n${seats.map((s) => s.join('')).join('\n')}\n`);
    },
  };
}

function simulate(seatLayout) {
  const seenSeatLayouts = new Set();
  seenSeatLayouts.add(seatLayout.stateKey());
  do {
    const prevSeatLayout = seatLayout;
    seatLayout = seatLayout.clone();
    for (let y = 0; y < seatLayout.height; y++) {
      for (let x = 0; x < seatLayout.width; x++) {
        const seat = seatLayout.get(x, y);
        if (seat === OCCUPIED) {
          const shouldBeEmpty = prevSeatLayout.shouldSeatBeEmpty(x, y);
          if (shouldBeEmpty) {
            seatLayout.set(x, y, EMPTY);
          }
        } else if (seat === EMPTY) {
          const shouldBeOccupied = prevSeatLayout.shouldSeatBeOccupied(x, y);
          if (shouldBeOccupied) {
            seatLayout.set(x, y, OCCUPIED);
          }
        }
      }
    }
    const stateKey = seatLayout.stateKey();
    if (seenSeatLayouts.has(stateKey)) {
      break;
    }
    seenSeatLayouts.add(stateKey);
  } while (true);

  return seatLayout;
}

export const part1 = (inputLines) => {
  const stableSeatLayout = simulate(
    toSeatLayout(inputLines, { useSight: false })
  );

  return stableSeatLayout
    .stateKey()
    .split('')
    .filter((v) => v === OCCUPIED).length;
};

export const part2 = (inputLines) => {
  const stableSeatLayout = simulate(
    toSeatLayout(inputLines, { useSight: true })
  );

  return stableSeatLayout
    .stateKey()
    .split('')
    .filter((v) => v === OCCUPIED).length;
};
