import { arrayUnique } from './utils/array.js';

const WHITE = '.';
const BLACK = '#';

const DIRECTIONS = ['e', 'se', 'sw', 'w', 'nw', 'ne'];

function Floor(blackTiles = new Set()) {
  return {
    blackTiles() {
      return Array.from(blackTiles);
    },
    getNeighbors(tileId) {
      const start = tileId.split(',').map((v) => parseInt(v, 10));
      return DIRECTIONS.map((p) => toCoordinate(p, [...start]));
    },
    getNeighborValues(tileId) {
      return this.getNeighbors(tileId).map((tileId) => this.getValue(tileId));
    },
    getValue(tileId) {
      return blackTiles.has(tileId) ? BLACK : WHITE;
    },
    setTile(tileId, value) {
      if (value === WHITE) {
        blackTiles.delete(tileId);
      } else {
        blackTiles.add(tileId);
      }
    },
    flipTile(tileId) {
      if (blackTiles.has(tileId)) {
        blackTiles.delete(tileId);
      } else {
        blackTiles.add(tileId);
      }
    },
  };
}

function toCoordinate(line, [q, r] = [0, 0]) {
  for (const direction of line.match(/(ne|nw|se|sw|e|w)/g)) {
    switch (direction) {
      case 'e':
        q += 1;
        break;
      case 'se':
        r += 1;
        break;
      case 'sw':
        q -= 1;
        r += 1;
        break;
      case 'w':
        q -= 1;
        break;
      case 'nw':
        r -= 1;
        break;
      case 'ne':
        q += 1;
        r -= 1;
        break;
    }
  }
  return [q, r].join(',');
}

export const part1 = (inputLines) => {
  const floor = Floor();
  inputLines.forEach((line) => floor.flipTile(toCoordinate(line)));
  return floor.blackTiles().filter((c) => floor.getValue(c) === BLACK).length;
};

export const part2 = (inputLines) => {
  let floor = Floor();
  inputLines.forEach((line) => floor.flipTile(toCoordinate(line)));
  let days = 100;
  while (days--) {
    let newFloor = Floor();
    const toCheck = arrayUnique(
      floor
        .blackTiles()
        .map((tileId) => floor.getNeighbors(tileId))
        .flat()
    );
    for (const tileId of toCheck) {
      if (floor.getValue(tileId) === BLACK) {
        const neighborsBlacks = floor
          .getNeighborValues(tileId)
          .filter((v) => v === BLACK).length;
        if (neighborsBlacks === 0 || neighborsBlacks > 2) {
          newFloor.setTile(tileId, WHITE);
        } else {
          newFloor.setTile(tileId, BLACK);
        }
      } else {
        const neighborsBlacks = floor
          .getNeighborValues(tileId)
          .filter((v) => v === BLACK).length;
        if (neighborsBlacks === 2) {
          newFloor.setTile(tileId, BLACK);
        } else {
          newFloor.setTile(tileId, WHITE);
        }
      }
    }
    floor = newFloor;
  }
  return floor.blackTiles().filter((c) => floor.getValue(c) === BLACK).length;
};
