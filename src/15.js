export const part1 = ([spokenString], { till = 2020 } = {}) => {
  const spokenNumbers = spokenString.split(',').map((v) => parseInt(v, 10));
  const spokenIndexes = new Map();
  spokenNumbers.forEach((n, i) => spokenIndexes.set(n, i + 1));

  let lastNumber = spokenNumbers[spokenNumbers.length - 1];
  let step = spokenNumbers.length;
  while (step < till) {
    let nextNumber = 0;
    if (spokenIndexes.has(lastNumber)) {
      nextNumber = step - spokenIndexes.get(lastNumber);
    }
    spokenIndexes.set(lastNumber, step++);
    lastNumber = nextNumber;
  }
  return lastNumber;
};

export const part2 = ([spokenString]) => {
  return part1([spokenString], { till: 30000000 });
};
