import { part1, part2 } from './17';

describe('Day 17 - Part 1', () => {
  it(`returns number of active cubes after 6 cycles`, () => {
    expect(part1(['.#.', '..#', '###'])).toBe(112);
  });
});

describe('Day 17 - Part 2', () => {
  it(`returns number of active hypercubes after 6 cycles`, () => {
    expect(part2(['.#.', '..#', '###'])).toBe(848);
  });
});
