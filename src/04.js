const FIELDS = {
  byr: (v) => {
    //(Birth Year) - four digits; at least 1920 and at most 2002.
    v = parseInt(v);
    return v >= 1920 && v <= 2002;
  },
  iyr: (v) => {
    // (Issue Year) - four digits; at least 2010 and at most 2020.
    v = parseInt(v);
    return v >= 2010 && v <= 2020;
  },
  eyr: (v) => {
    // (Expiration Year) - four digits; at least 2020 and at most 2030.
    v = parseInt(v);
    return v >= 2020 && v <= 2030;
  },
  hgt: (v) => {
    // (Height) - a number followed by either cm or in:
    // If cm, the number must be at least 150 and at most 193.
    if (v.includes('cm')) {
      v = parseInt(v.replace(/[^\d]/g, ''));
      return v >= 150 && v <= 193;
      // If in, the number must be at least 59 and at most 76.
    } else if (v.includes('in')) {
      v = parseInt(v.replace(/[^\d]/g, ''));
      return v >= 59 && v <= 76;
    } else {
      return false;
    }
  },
  hcl: (v) => {
    // (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
    return /^#[0-9a-f]{6}$/.test(v);
  },
  ecl: (v) => {
    // (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
    return ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'].includes(v);
  },
  pid: (v) => {
    // (Passport ID) - a nine-digit number, including leading zeroes.
    return /^\d{9}$/.test(v);
  },
};

function parse(inputLines) {
  const parsedData = [];
  let lines = [];
  for (const line of inputLines) {
    if (line === '') {
      parsedData.push(lines.join(' '));
      lines = [];
    } else {
      lines.push(line);
    }
  }
  if (lines.length) {
    parsedData.push(lines.join(' '));
  }
  return parsedData;
}

function toPassport(line) {
  const fieldData = line.split(' ');
  const passport = {};
  for (const data of fieldData) {
    const [key, value] = data.split(':');
    passport[key] = value;
  }
  return passport;
}

function hasRequiredFields(passport, requiredFields) {
  return requiredFields.every((f) => f in passport);
}

export const part1 = (inputLines) => {
  const passportData = parse(inputLines);
  const passports = passportData.map((d) => toPassport(d));
  const validFields = Object.keys(FIELDS).filter((v) => v !== 'cid');
  return passports.filter((p) => hasRequiredFields(p, validFields)).length;
};

function isValid(passport) {
  return Object.keys(FIELDS).every(
    (k) => k in passport && FIELDS[k](passport[k])
  );
}

export const part2 = (inputLines) => {
  const passportData = parse(inputLines);
  const passports = passportData.map((d) => toPassport(d));
  return passports.filter((p) => isValid(p)).length;
};
