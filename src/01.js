function getTwoValuesAddedTo(values, addsTo) {
  for (let i = 0; i < values.length; i++) {
    if (values[i] > addsTo) continue;
    for (let j = i + 1; j < values.length; j++) {
      if (values[i] + values[j] === addsTo) {
        return [values[i], values[j]];
      }
    }
  }
  return [0, 0];
}

function getThreeValuesAddedTo(values, addsTo) {
  for (let i = 0; i < values.length; i++) {
    if (values[i] > addsTo) continue;
    for (let j = i + 1; j < values.length; j++) {
      if (values[i] + values[j] > addsTo) continue;
      for (let k = j + 1; k < values.length; k++) {
        if (values[i] + values[j] + values[k] === addsTo) {
          return [values[i], values[j], values[k]];
        }
      }
    }
  }
  return [0, 0, 0];
}

export const part1 = (inputLines) => {
  const values = inputLines.map((v) => parseInt(v, 10));
  const [v1, v2] = getTwoValuesAddedTo(values, 2020);
  return v1 * v2;
};

export const part2 = (inputLines) => {
  const values = inputLines.map((v) => parseInt(v, 10));
  const [v1, v2, v3] = getThreeValuesAddedTo(values, 2020);
  return v1 * v2 * v3;
};
