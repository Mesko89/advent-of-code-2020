import { toSeatId, part1, part2 } from './05';

describe('Day 05 - Part 1', () => {
  const testData = [
    { seat: 'FBFBBFFRLR', expectedSeatId: 357 },
    { seat: 'BFFFBBFRRR', expectedSeatId: 567 },
    { seat: 'FFFBBBFRRR', expectedSeatId: 119 },
    { seat: 'BBFFBBFRLL', expectedSeatId: 820 },
  ];

  for (const test of testData) {
    it(`gets correct seat ID for ${test.seat}`, () => {
      expect(toSeatId(test.seat)).toBe(test.expectedSeatId);
    });
  }

  it(`gets highest seat ID`, () => {
    expect(part1(testData.map((d) => d.seat))).toBe(820);
  });
});

describe('Day 05 - Part 2', () => {
  it(`gets missing seat ID`, () => {
    expect(
      part2([
        'FFBBFFFLLL',
        'FFBBFFFLLR',
        'FFBBFFFLRL',
        'FFBBFFFLRR',
        'FFBBFFFRLL',
        'FFBBFFFRRL',
        'FFBBFFFRRR',
      ])
    ).toBe(197);
  });
});
