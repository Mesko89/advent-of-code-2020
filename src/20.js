import {
  rotateGridRight,
  flipGridVertical,
  flipGridHorizontal,
} from './utils/grid.js';

function gridHash(grid) {
  return grid.map((v) => v.join('')).join('\n');
}

function getEdges(grid) {
  return {
    top: grid[0],
    right: grid.map((v) => v[v.length - 1]),
    bottom: grid[grid.length - 1],
    left: grid.map((v) => v[0]),
  };
}

export function toPuzzle(inputLines) {
  inputLines.reverse();
  const tiles = [];
  let line = '';
  while ((line = inputLines.pop())) {
    if (line.startsWith('Tile ')) {
      const tileId = line.replace('Tile ', '').replace(':', '');
      const variants = [];
      const grid = [];
      while ((line = inputLines.pop())) {
        grid.push(line.split(''));
      }
      const edges = getEdges(grid);
      const baseVariant = { tileId, grid, edges };
      const uniqueVariants = new Set();
      uniqueVariants.add(gridHash(baseVariant.grid));
      variants.push(baseVariant);
      const addToVariants = (tile) => {
        const hash = gridHash(tile.grid);
        if (!uniqueVariants.has(hash)) {
          uniqueVariants.add(hash);
          variants.push(tile);
        }
      };

      for (let i = 1; i < 4; i++) {
        let tile = rotateRight(baseVariant, i);
        addToVariants(tile);
        addToVariants(flipVertical(tile));
        addToVariants(flipHorizontal(tile));
      }
      tiles.push({ tileId, variants });
    }
  }
  return tiles;
}

function flipVertical(tile) {
  const grid = flipGridVertical(tile.grid);
  return {
    tileId: tile.tileId,
    grid,
    edges: getEdges(grid),
  };
}

function flipHorizontal(tile) {
  const grid = flipGridHorizontal(tile.grid);
  return {
    tileId: tile.tileId,
    grid,
    edges: getEdges(grid),
  };
}

function rotateRight(tile, times) {
  const grid = rotateGridRight(tile.grid, times);
  return {
    tileId: tile.tileId,
    grid,
    edges: getEdges(grid),
  };
}

function areSameEdges(edgeA, edgeB) {
  for (let i = 0; i < edgeA.length; i++) {
    if (edgeA[i] !== edgeB[i]) return false;
  }
  return true;
}

function getMatchedVariants(tile, constraints) {
  const variants = [];
  for (const variant of tile.variants) {
    let isMatched = true;
    for (const [edge, value] of Object.entries(constraints)) {
      if (!areSameEdges(variant.edges[edge], value)) {
        isMatched = false;
        break;
      }
    }
    if (isMatched) {
      variants.push(variant);
    }
  }
  return variants;
}

function getConstraints(grid, [x, y]) {
  const constraints = {};
  if (grid[`${x},${y - 1}`]) {
    constraints.top = grid[`${x},${y - 1}`].edges.bottom;
  }
  if (grid[`${x},${y + 1}`]) {
    constraints.bottom = grid[`${x},${y + 1}`].edges.top;
  }
  if (grid[`${x - 1},${y}`]) {
    constraints.left = grid[`${x - 1},${y}`].edges.right;
  }
  if (grid[`${x + 1},${y}`]) {
    constraints.right = grid[`${x + 1},${y}`].edges.left;
  }
  return constraints;
}

function getEmptySpaces(grid, squareSize, [x, y]) {
  return [
    [x, y - 1],
    [x, y + 1],
    [x - 1, y],
    [x + 1, y],
  ].filter(
    ([x, y]) =>
      x >= 0 &&
      y >= 0 &&
      x < squareSize &&
      y < squareSize &&
      !Boolean(grid[`${x},${y}`])
  );
}

function analyzePuzzle(puzzle) {
  const corners = [];
  for (const tile of puzzle) {
    const matchedEdges = new Set();
    for (const [edge, value] of Object.entries(tile.variants[0].edges)) {
      for (const tile2 of puzzle) {
        if (tile2.tileId === tile.tileId) continue;
        const matchedVariants = getMatchedVariants(tile2, {
          [edge]: value,
        });
        if (matchedVariants.length) {
          matchedEdges.add(edge);
          break;
        }
      }
    }
    if (matchedEdges.size === 2) {
      tile.isCorner = true;
    } else if (matchedEdges.size === 3) {
      tile.isEdge = true;
    }
  }
  return corners;
}

function isCorner(puzzle, [x, y]) {
  const lastIndex = Math.sqrt(puzzle) - 1;
  return (
    (x === 0 && y === 0) ||
    (x === 0 && y === lastIndex) ||
    (x === lastIndex && y === 0) ||
    (x === lastIndex && y === lastIndex)
  );
}

function solve(
  puzzle,
  grid = {},
  emptySpaces = [[0, 0]],
  placedTiles = new Set()
) {
  if (puzzle.length === placedTiles.size) return grid;
  for (const emptySpace of emptySpaces) {
    const constraints = getConstraints(grid, emptySpace);
    const isOnCorner = isCorner(puzzle, emptySpace);
    const possibleTiles = puzzle
      .filter((tile) => {
        if (placedTiles.has(tile.tileId)) {
          return false;
        }
        if (isOnCorner) {
          return tile.isCorner;
        }
        return true;
      })
      .map((tile) => ({
        tileId: tile.tileId,
        matches: getMatchedVariants(tile, constraints),
      }))
      .sort((a, b) => a.matches.length - b.matches.length);
    for (const possibleTile of possibleTiles) {
      for (const match of possibleTile.matches) {
        const newGrid = {
          ...grid,
          [`${emptySpace[0]},${emptySpace[1]}`]: match,
        };
        const newPlacedTiles = new Set(placedTiles);
        newPlacedTiles.add(match.tileId);
        const newEmptySpaces = [
          ...emptySpaces.filter((v) => v !== emptySpace),
          ...getEmptySpaces(newGrid, Math.sqrt(puzzle.length), emptySpace),
        ];
        const v = solve(puzzle, newGrid, newEmptySpaces, newPlacedTiles);
        if (v) {
          return v;
        }
      }
    }
  }
}

function toImage(grid, squareSize) {
  const tileSize = grid['0,0'].grid.length - 2;
  const combinedGrid = Array.from({
    length: tileSize * squareSize,
  }).map(() => []);
  for (let y = 0; y < squareSize; y++) {
    for (let x = 0; x < squareSize; x++) {
      const tileGrid = grid[`${x},${y}`].grid;
      for (let sy = 1; sy < tileSize + 1; sy++) {
        for (let sx = 1; sx < tileSize + 1; sx++) {
          combinedGrid[tileSize * y + (sy - 1)].push(tileGrid[sy][sx]);
        }
      }
    }
  }
  return combinedGrid;
}

const monster = [
  '                  # '.split(''),
  '#    ##    ##    ###'.split(''),
  ' #  #  #  #  #  #   '.split(''),
];

function locateMonsters(image, monsterMark = 'O') {
  const variants = [
    (image) => image,
    (image) => rotateGridRight(image, 1),
    (image) => rotateGridRight(image, 2),
    (image) => rotateGridRight(image, 3),
    (image) => flipGridVertical(image),
    (image) => flipGridHorizontal(image),
    (image) => flipGridVertical(rotateGridRight(image, 1)),
    (image) => flipGridHorizontal(rotateGridRight(image, 1)),
  ];
  for (const variant of variants) {
    const variantImage = variant(image);
    let monsterFound = false;
    for (let y = 0; y < variantImage.length - monster.length + 2; y++) {
      for (let x = 0; x < variantImage[y].length - monster[0].length + 2; x++) {
        const isMonsterLocated = monster.every((l, my) =>
          l.every((v, mx) => {
            if (v === ' ') return true;
            else return ['O', '#'].includes(variantImage[y + my][x + mx]);
          })
        );
        if (isMonsterLocated) {
          monsterFound = true;
          monster.forEach((l, my) =>
            l.forEach((v, mx) => {
              if (v !== ' ') {
                variantImage[y + my][x + mx] = monsterMark;
              }
            })
          );
        }
      }
    }
    if (monsterFound) {
      return variantImage;
    }
  }
}

export const part1 = ([...inputLines]) => {
  const puzzle = toPuzzle(inputLines);
  analyzePuzzle(puzzle);
  const solution = solve(puzzle);
  const { minX, maxX, minY, maxY } = {
    minX: 0,
    maxX: Math.sqrt(puzzle.length) - 1,
    minY: 0,
    maxY: Math.sqrt(puzzle.length) - 1,
  };
  return [
    solution[`${minX},${minY}`],
    solution[`${minX},${maxY}`],
    solution[`${maxX},${minY}`],
    solution[`${maxX},${maxY}`],
  ].reduce((total, tile) => total * parseInt(tile.tileId), 1);
};

export const part2 = ([...inputLines]) => {
  const puzzle = toPuzzle(inputLines);
  analyzePuzzle(puzzle);
  const solution = solve(puzzle);
  const image = toImage(solution, Math.sqrt(puzzle.length));
  const radarImage = locateMonsters(image);
  return radarImage.reduce(
    (total, l) => total + l.filter((v) => v === '#').length,
    0
  );
};
