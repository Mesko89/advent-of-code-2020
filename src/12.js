import { manhattanDistance } from './utils/distance.js';

function move({ pos, direction, value }) {
  pos.x = pos.x + value * direction.x;
  pos.y = pos.y + value * direction.y;
  return pos;
}

const TURN_LEFT = {
  '1,0': { x: 0, y: -1 },
  '0,-1': { x: -1, y: 0 },
  '-1,0': { x: 0, y: 1 },
  '0,1': { x: 1, y: 0 },
};

const TURN_RIGHT = {
  '1,0': { x: 0, y: 1 },
  '0,1': { x: -1, y: 0 },
  '-1,0': { x: 0, y: -1 },
  '0,-1': { x: 1, y: 0 },
};

function turnLeft(direction, value) {
  return turnRight(direction, 360 - (value % 360));
}

function turnRight(direction, value) {
  value = value % 360;
  switch (value) {
    case 90:
      return { x: -direction.y, y: direction.x };
    case 180:
      return { x: -direction.x, y: -direction.y };
    case 270:
      return { x: direction.y, y: -direction.x };
  }
}

const COMMANDS = {
  N: ({ pos, direction, value }) => {
    pos = move({ pos, direction: { x: 0, y: -1 }, value });
    return { pos, direction };
  },
  S: ({ pos, direction, value }) => {
    pos = move({ pos, direction: { x: 0, y: 1 }, value });
    return { pos, direction };
  },
  E: ({ pos, direction, value }) => {
    pos = move({ pos, direction: { x: 1, y: 0 }, value });
    return { pos, direction };
  },
  W: ({ pos, direction, value }) => {
    pos = move({ pos, direction: { x: -1, y: 0 }, value });
    return { pos, direction };
  },
  L: ({ pos, direction, value }) => {
    direction = turnLeft(direction, value);
    return { pos, direction };
  },
  R: ({ pos, direction, value }) => {
    direction = turnRight(direction, value);
    return { pos, direction };
  },
  F: ({ pos, direction, value }) => {
    pos = move({ pos, direction, value });
    return { pos, direction };
  },
};

function parseInstructions(inputLines) {
  return inputLines.map((line) => ({
    command: line[0],
    value: parseInt(line.substring(1)),
  }));
}

function simulateInstructions(instructions) {
  let pos = { x: 0, y: 0 };
  let direction = { x: 1, y: 0 };
  for (const { command, value } of instructions) {
    switch (command) {
      case 'N':
        pos = move({ pos, direction: { x: 0, y: -1 }, value });
        break;
      case 'S':
        pos = move({ pos, direction: { x: 0, y: 1 }, value });
        break;
      case 'E':
        pos = move({ pos, direction: { x: 1, y: 0 }, value });
        break;
      case 'W':
        pos = move({ pos, direction: { x: -1, y: 0 }, value });
        break;
      case 'L':
        direction = turnLeft(direction, value);
        break;
      case 'R':
        direction = turnRight(direction, value);
        break;
      case 'F':
        pos = move({ pos, direction, value });
        break;
    }
  }
  return pos;
}

function simulateRealInstructions(instructions) {
  let pos = { x: 0, y: 0 };
  let waypoint = { x: 10, y: -1 };
  for (const { command, value } of instructions) {
    switch (command) {
      case 'N':
        waypoint = move({ pos: waypoint, direction: { x: 0, y: -1 }, value });
        break;
      case 'S':
        waypoint = move({ pos: waypoint, direction: { x: 0, y: 1 }, value });
        break;
      case 'E':
        waypoint = move({ pos: waypoint, direction: { x: 1, y: 0 }, value });
        break;
      case 'W':
        waypoint = move({ pos: waypoint, direction: { x: -1, y: 0 }, value });
        break;
      case 'L':
        waypoint = turnLeft(waypoint, value);
        break;
      case 'R':
        waypoint = turnRight(waypoint, value);
        break;
      case 'F':
        pos = move({ pos, direction: waypoint, value });
        break;
    }
  }
  return pos;
}

export const part1 = (inputLines) => {
  const instructions = parseInstructions(inputLines);
  const endPosition = simulateInstructions(instructions);
  return manhattanDistance({ x: 0, y: 0 }, endPosition);
};

export const part2 = (inputLines) => {
  const instructions = parseInstructions(inputLines);
  const endPosition = simulateRealInstructions(instructions);
  return manhattanDistance({ x: 0, y: 0 }, endPosition);
};
