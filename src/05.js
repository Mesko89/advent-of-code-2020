import { arrayMax } from './utils/array.js';

export function toSeatId(seat) {
  const row = seat.substring(0, 7).replace(/B/g, '1').replace(/F/g, '0');
  const column = seat.substring(7).replace(/R/g, '1').replace(/L/g, '0');
  return parseInt(row, 2) * 8 + parseInt(column, 2);
}

function findMissingSeat([...seats]) {
  // important as we want to check for missing gaps
  seats.sort((a, b) => a - b);

  let nextSeatId = 0;
  for (const seat of seats) {
    // Set to the first seat as seats before that are missing
    if (nextSeatId === 0) {
      nextSeatId = seat;
    } else {
      nextSeatId++;
      // Check if seat is not next seatId
      if (nextSeatId < seat) {
        return nextSeatId;
      }
    }
  }
}

export const part1 = (inputLines) => {
  const seats = inputLines.map((d) => toSeatId(d));
  return arrayMax(seats);
};

export const part2 = (inputLines) => {
  const seats = inputLines.map((d) => toSeatId(d)).sort((a, b) => a - b);
  return findMissingSeat(seats);
};
