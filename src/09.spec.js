import { part1, part2 } from './09';

describe('Day 09 - Part 1', () => {
  it(`finds first invalid number`, () => {
    expect(
      part1(
        [
          '35',
          '20',
          '15',
          '25',
          '47',
          '40',
          '62',
          '55',
          '65',
          '95',
          '102',
          '117',
          '150',
          '182',
          '127',
          '219',
          '299',
          '277',
          '309',
          '576',
        ],
        5
      )
    ).toBe(127);
  });
});

describe('Day 09 - Part 2', () => {
  it(`gets encryption weakness in XMAS-encrypted list of numbers`, () => {
    expect(
      part2(
        [
          '35',
          '20',
          '15',
          '25',
          '47',
          '40',
          '62',
          '55',
          '65',
          '95',
          '102',
          '117',
          '150',
          '182',
          '127',
          '219',
          '299',
          '277',
          '309',
          '576',
        ],
        5
      )
    ).toBe(62);
  });
});
