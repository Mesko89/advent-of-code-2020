function toMenu(inputLines) {
  return inputLines.map((line) => {
    const ingredients = line.substring(0, line.indexOf(' (')).split(' ');
    const allergens = line
      .match(/\((.*)\)$/)[1]
      .replace('contains ', '')
      .split(', ');
    return { ingredients, allergens };
  });
}

function matchAllergens(menu) {
  const allergensMap = {};
  for (const { ingredients, allergens } of menu) {
    for (const allergen of allergens) {
      if (allergen in allergensMap) {
        allergensMap[allergen] = ingredients.filter((ingredient) =>
          allergensMap[allergen].includes(ingredient)
        );
      } else {
        allergensMap[allergen] = ingredients;
      }
    }
  }

  return allergensMap;
}

function solveAllergens(allergensMap) {
  const foundAllergens = new Map();
  let exactMatch = null;
  while (
    (exactMatch = Object.entries(allergensMap).find(([_, v]) => v.length === 1))
  ) {
    const ingredient = exactMatch[1][0];
    foundAllergens.set(exactMatch[0], ingredient);
    for (const [allergen, ingredients] of Object.entries(allergensMap)) {
      allergensMap[allergen] = ingredients.filter((i) => i !== ingredient);
    }
  }
  return foundAllergens;
}

export const part1 = ([...inputLines]) => {
  const menu = toMenu(inputLines);
  const allergensMap = matchAllergens(menu);
  const ingredientsWithAllergen = Array.from(
    new Set(Object.values(allergensMap).flat())
  );
  return menu
    .map((m) => m.ingredients)
    .flat()
    .filter((ingredient) => !ingredientsWithAllergen.includes(ingredient))
    .length;
};

export const part2 = ([...inputLines]) => {
  const menu = toMenu(inputLines);
  const allergensMap = matchAllergens(menu);
  const solution = solveAllergens(allergensMap);
  return Array.from(solution.entries())
    .sort(([A], [B]) => (A > B ? 1 : A < B ? -1 : 0))
    .map((v) => v[1])
    .join(',');
};
