function pos(...coordinates) {
  return coordinates.join(',');
}

function parsePos(str) {
  const [x, y, z, w] = str.split(',').map((v) => parseInt(v));
  if (typeof w !== 'undefined') {
    return { x, y, z, w };
  }
  return { x, y, z };
}

function getNeighbors(coordinate) {
  const p = parsePos(coordinate);
  const neighbors = [];
  for (let x = p.x - 1; x <= p.x + 1; x++) {
    for (let y = p.y - 1; y <= p.y + 1; y++) {
      for (let z = p.z - 1; z <= p.z + 1; z++) {
        if ('w' in p) {
          for (let w = p.w - 1; w <= p.w + 1; w++) {
            if (x !== p.x || y !== p.y || z !== p.z || w !== p.w) {
              neighbors.push(pos(x, y, z, w));
            }
          }
        } else {
          if (x !== p.x || y !== p.y || z !== p.z) {
            neighbors.push(pos(x, y, z));
          }
        }
      }
    }
  }
  return neighbors;
}

function getActiveCubes(inputLines, dimensions) {
  const activeCubes = new Set();
  inputLines.forEach((line, y) => {
    line.split('').forEach((v, x) => {
      if (v === '#') {
        activeCubes.add(
          pos(x, y, ...Array.from({ length: dimensions - 2 }).fill(0))
        );
      }
    });
  });
  return activeCubes;
}

function step(activeCubes, getNeighbors) {
  const newActiveCubes = new Set();
  const cubesToCheck = [];
  activeCubes.forEach((cube) => cubesToCheck.push(...getNeighbors(cube)));
  for (const cube of cubesToCheck) {
    if (activeCubes.has(cube)) {
      const totalActiveNeighbors = getNeighbors(cube).filter((c) =>
        activeCubes.has(c)
      ).length;
      if (totalActiveNeighbors === 2 || totalActiveNeighbors === 3) {
        newActiveCubes.add(cube);
      }
    } else {
      const totalActiveNeighbors = getNeighbors(cube).filter((c) =>
        activeCubes.has(c)
      ).length;
      if (totalActiveNeighbors === 3) {
        newActiveCubes.add(cube);
      }
    }
  }
  return newActiveCubes;
}

export const part1 = (inputLines) => {
  let activeCubes = getActiveCubes(inputLines, 3);
  let steps = 6;
  while (steps--) {
    activeCubes = step(activeCubes, getNeighbors);
  }
  return activeCubes.size;
};

export const part2 = (inputLines) => {
  let activeCubes = getActiveCubes(inputLines, 4);
  let steps = 6;
  while (steps--) {
    activeCubes = step(activeCubes, getNeighbors);
  }
  return activeCubes.size;
};
