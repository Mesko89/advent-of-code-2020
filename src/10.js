function findAdapterDifferences(adapters) {
  adapters = [0, ...adapters].sort((a, b) => a - b);
  adapters = [...adapters, adapters[adapters.length - 1] + 3];
  const diffs = { [0]: 0, [1]: 0, [2]: 0, [3]: 0 };
  for (let i = 1; i < adapters.length; i++) {
    const diff = adapters[i] - adapters[i - 1];
    diffs[diff]++;
  }
  return diffs;
}

function getGroups(adapters) {
  adapters = [0, ...adapters].sort((a, b) => a - b);
  const groups = [];
  let group = [];
  for (let i = 0; i < adapters.length; i++) {
    if (group.length === 0 || adapters[i] - adapters[i - 1] === 1) {
      group.push(adapters[i]);
    } else {
      groups.push(group);
      group = [adapters[i]];
    }
  }
  groups.push(group);
  return groups;
}

function tribonacci(val) {
  let values = [0, 0, 1];
  if (val === 1) return 1;
  val--;
  while (val--) {
    values.push(values.slice(-3).reduce((a, b) => a + b));
  }
  return values[values.length - 1];
}

export const part1 = (inputLines) => {
  const adapters = inputLines.map((v) => parseInt(v, 10));
  const diffs = findAdapterDifferences(adapters);
  return diffs['1'] * diffs['3'];
};

export const part2 = (inputLines) => {
  const adapters = inputLines.map((v) => parseInt(v, 10));
  const groups = getGroups(adapters);
  return groups.reduce((total, group) => {
    return total * tribonacci(group.length);
  }, 1);
};
