function toPlayers(inputLines) {
  inputLines = inputLines.reverse();
  const player1 = [];
  const player2 = [];
  let line = '';

  while ((line = inputLines.pop())) {
    if (line.startsWith('Player')) continue;
    player1.push(parseInt(line, 10));
  }

  while ((line = inputLines.pop())) {
    if (line.startsWith('Player')) continue;
    player2.push(parseInt(line, 10));
  }
  return { player1, player2 };
}

function score(deck) {
  let factor = deck.length;
  let total = 0;
  for (let i = 0; i < deck.length; i++, factor--) {
    total += deck[i] * factor;
  }
  return total;
}

function combat([...player1], [...player2]) {
  while (player1.length !== 0 && player2.length !== 0) {
    const player1Card = player1.shift();
    const player2Card = player2.shift();
    if (player1Card > player2Card) {
      player1.push(player1Card, player2Card);
    } else {
      player2.push(player2Card, player1Card);
    }
  }
  return { player1, player2 };
}

const gameHash = (p1, p2) => `${p1.join(',')}|${p2.join(',')}`;

function recursiveCombat([...player1], [...player2]) {
  const states = new Set();

  while (player1.length !== 0 && player2.length !== 0) {
    const hash = gameHash(player1, player2);

    if (states.has(hash)) {
      return { player1, player2: [] };
    } else {
      states.add(hash);
    }

    const player1Card = player1.shift();
    const player2Card = player2.shift();

    if (player1Card <= player1.length && player2Card <= player2.length) {
      const result = recursiveCombat(
        player1.slice(0, player1Card),
        player2.slice(0, player2Card)
      );

      if (result.player1.length) {
        player1.push(player1Card, player2Card);
      } else {
        player2.push(player2Card, player1Card);
      }
    } else if (player1Card > player2Card) {
      player1.push(player1Card, player2Card);
    } else {
      player2.push(player2Card, player1Card);
    }
  }

  return { player1, player2 };
}

export const part1 = ([...inputLines]) => {
  const players = toPlayers(inputLines);
  const { player1, player2 } = combat(players.player1, players.player2);

  if (player1.length) return score(player1);
  else return score(player2);
};

export const part2 = ([...inputLines]) => {
  const players = toPlayers(inputLines);
  const { player1, player2 } = recursiveCombat(
    players.player1,
    players.player2
  );
  if (player1.length) return score(player1);
  else return score(player2);
};
