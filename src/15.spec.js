import { part1, part2 } from './15';

describe('Day 15 - Part 1', () => {
  const tests = [
    { input: ['0,3,6'], result: 436 },
    { input: ['1,3,2'], result: 1 },
    { input: ['2,1,3'], result: 10 },
    { input: ['1,2,3'], result: 27 },
    { input: ['2,3,1'], result: 78 },
    { input: ['3,2,1'], result: 438 },
    { input: ['3,1,2'], result: 1836 },
  ];
  for (const { input, result } of tests) {
    it(`returns 2020th number spoken for "${input.join(',')}"`, () => {
      expect(part1(input)).toBe(result);
    });
  }
});

describe('Day 15 - Part 2', () => {
  const tests = [
    { input: ['0,3,6'], result: 175594 },
    { input: ['1,3,2'], result: 2578 },
    { input: ['2,1,3'], result: 3544142 },
    { input: ['1,2,3'], result: 261214 },
    { input: ['2,3,1'], result: 6895259 },
    { input: ['3,2,1'], result: 18 },
    { input: ['3,1,2'], result: 362 },
  ];
  for (const { input, result } of tests) {
    it(`returns 30000000th number spoken for "${input.join(',')}"`, () => {
      expect(part2(input)).toBe(result);
    });
  }
});
