import { part1, part2 } from './10';

describe('Day 10 - Part 1', () => {
  it(`returns the number of 1-jolt differences multiplied by the number of 3-jolt differences`, () => {
    expect(
      part1(['16', '10', '15', '5', '1', '11', '7', '19', '6', '12', '4'])
    ).toBe(7 * 5);
  });
});

describe('Day 10 - Part 2', () => {
  it(`returns total number of distinct ways to arrange the adapters`, () => {
    expect(
      part2(['16', '10', '15', '5', '1', '11', '7', '19', '6', '12', '4'], 5)
    ).toBe(8);
  });

  it(`returns total number of distinct ways to arrange the adapters (higher)`, () => {
    expect(
      part2([
        '28',
        '33',
        '18',
        '42',
        '31',
        '14',
        '46',
        '20',
        '48',
        '47',
        '24',
        '23',
        '49',
        '45',
        '19',
        '38',
        '39',
        '11',
        '1',
        '32',
        '25',
        '35',
        '8',
        '17',
        '7',
        '9',
        '4',
        '2',
        '34',
        '10',
        '3',
      ])
    ).toBe(19208);
  });
});
