function maskValue(value, mask) {
  return value
    .split('')
    .map((v, i) => {
      if (mask[i] === 'X') {
        return v;
      } else {
        return mask[i];
      }
    })
    .join('');
}

function expandAddress(address) {
  const xIndex = address.indexOf('X');
  if (xIndex === -1) return [address];
  return [
    ...expandAddress(
      address.substring(0, xIndex) + '0' + address.substring(xIndex + 1)
    ),
    ...expandAddress(
      address.substring(0, xIndex) + '1' + address.substring(xIndex + 1)
    ),
  ];
}

function getMaskedAddresses(address, mask) {
  const mappedAddress = address
    .split('')
    .map((v, i) => {
      if (mask[i] === 'X') {
        return 'X';
      } else if (mask[i] === '0') {
        return v;
      } else {
        return '1';
      }
    })
    .join('');
  return expandAddress(mappedAddress);
}

export const part1 = (inputLines) => {
  const memory = {};
  let currentMask = 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX';
  for (const line of inputLines) {
    if (line.startsWith('mask')) {
      currentMask = line.replace('mask = ', '');
    } else {
      const [_, address, value] = line
        .match(/mem\[(\d+)] = (\d+)/)
        .map((v) => parseInt(v, 10));
      memory[address] = maskValue(
        value.toString(2).padStart(36, '0'),
        currentMask
      );
    }
  }
  return Object.values(memory).reduce(
    (total, value) => total + parseInt(value, 2),
    0
  );
};

export const part2 = (inputLines) => {
  const memory = {};
  let currentMask = 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX';
  for (const line of inputLines) {
    if (line.startsWith('mask')) {
      currentMask = line.replace('mask = ', '');
    } else {
      const [_, address, value] = line
        .match(/mem\[(\d+)] = (\d+)/)
        .map((v) => parseInt(v, 10));
      const addresses = getMaskedAddresses(
        address.toString(2).padStart(36, '0'),
        currentMask
      );
      for (const address of addresses) {
        memory[address] = value.toString(2).padStart(36, '0');
      }
    }
  }
  return Object.values(memory).reduce(
    (total, value) => total + parseInt(value, 2),
    0
  );
};
