import { part1, part2 } from './23';

describe('Day 23 - Part 1', () => {
  it(`returns labels after cup #1`, () => {
    expect(part1(['389125467'], 10)).toBe('92658374');
  });
});

describe('Day 23 - Part 2', () => {
  it(`returns multiply of values of cups containing stars`, () => {
    expect(part2(['389125467'])).toBe(149245887792);
  });
});
