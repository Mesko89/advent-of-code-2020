const OPEN = '.';
const TREE = '#';

function toForest(inputLines) {
  return inputLines.map((l) => l.split(''));
}

function getEncounteredTrees(forest, slope) {
  let totalTrees = 0;
  let x = 0;
  for (let y = 0; y < forest.length; y += slope.y) {
    const value = forest[y][x];
    if (value === TREE) {
      totalTrees++;
    }
    x = (x + slope.x) % forest[y].length;
  }
  return totalTrees;
}

export const part1 = (inputLines) => {
  const forest = toForest(inputLines);
  return getEncounteredTrees(forest, { x: 3, y: 1 });
};

export const part2 = (inputLines) => {
  const forest = toForest(inputLines);
  return [
    { x: 1, y: 1 },
    { x: 3, y: 1 },
    { x: 5, y: 1 },
    { x: 7, y: 1 },
    { x: 1, y: 2 },
  ]
    .map((slope) => getEncounteredTrees(forest, slope))
    .reduce((a, b) => a * b);
};
