import { part1, part2 } from './14';

describe('Day 14 - Part 1', () => {
  it(`returns sum of values in memory`, () => {
    expect(
      part1([
        'mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X',
        'mem[8] = 11',
        'mem[7] = 101',
        'mem[8] = 0',
      ])
    ).toBe(165);
  });
});

describe('Day 14 - Part 2', () => {
  it(`returns sum of values in memory`, () => {
    expect(
      part2([
        'mask = 000000000000000000000000000000X1001X',
        'mem[42] = 100',
        'mask = 00000000000000000000000000000000X0XX',
        'mem[26] = 1',
      ])
    ).toBe(208);
  });
});
