import { part1, part2 } from './22';

describe('Day 22 - Part 1', () => {
  it(`returns score of winning player`, () => {
    expect(
      part1([
        'Player 1:',
        '9',
        '2',
        '6',
        '3',
        '1',
        '',
        'Player 2:',
        '5',
        '8',
        '4',
        '7',
        '10',
      ])
    ).toBe(306);
  });
});

describe('Day 22 - Part 2', () => {
  it(`returns score of winning player`, () => {
    expect(
      part2([
        'Player 1:',
        '9',
        '2',
        '6',
        '3',
        '1',
        '',
        'Player 2:',
        '5',
        '8',
        '4',
        '7',
        '10',
      ])
    ).toBe(291);
  });

  it(`returns score of winning player`, () => {
    expect(
      part2(['Player 1:', '43', '19', '', 'Player 2:', '2', '29', '14'])
    ).toBe(105);
  });
});
